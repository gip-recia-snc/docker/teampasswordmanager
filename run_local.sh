#!/bin/bash

docker run -d -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=tpm -e MYSQL_USER=tpm -e MYSQL_PASSWORD=tpm --name mysql mysql:5
docker run -d -p 8080:80 --link mysql:mysql --name tpm tpm
