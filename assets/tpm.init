#!/bin/bash

# Basic variables
SETUP_DIR="/opt/tpm-setup"
CONFIG_TEMPLATES_DIR="${SETUP_DIR}/config"

# Database
DB_HOST=${DB_HOST:-}
DB_USER=${DB_USER:-}
DB_PASS=${DB_PASS:-}
DB_NAME=${DB_NAME:-}
REVERSE_PROXY=${REVERSE_PROXY:-}
BASE_URL=${BASE_URL}

if [ -n "${MYSQL_PORT_3306_TCP_ADDR}" ]; then
    DB_HOST=${DB_HOST:-${MYSQL_PORT_3306_TCP_ADDR}:${MYSQL_PORT_3306_TCP_PORT}}
fi

# Set default values for database
DB_HOST=${DB_HOST:-localhost}
DB_USER=${DB_USER:-tpm}
DB_PASS=${DB_PASS:-tpm}
DB_NAME=${DB_NAME:-tpm}

# Copy configuration file if not Empty
if [ ! -s "/var/www/html/config.php" ]; then 
	cp ${CONFIG_TEMPLATES_DIR}/config.php /var/www/html/config.php

	# Configure database
	sed 's/{{DB_HOST}}/'"${DB_HOST}"'/' -i /var/www/html/config.php
	sed 's/{{DB_USER}}/'"${DB_USER}"'/' -i /var/www/html/config.php
	sed 's/{{DB_PASS}}/'"${DB_PASS}"'/' -i /var/www/html/config.php
	sed 's/{{DB_NAME}}/'"${DB_NAME}"'/' -i /var/www/html/config.php

	# Configure reverse proxy
	if [ -n "${REVERSE_PROXY}" ]; then
	    echo "" >> /var/www/html/config.php
	    echo "// Reverse Proxy IP" >> /var/www/html/config.php
	    echo "define('REVERSE_PROXY', '"${REVERSE_PROXY}"');" >> /var/www/html/config.php
	fi

	# Configure base url
	if [ -n "${BASE_URL}" ]; then
	    echo "" >> /var/www/html/config.php
	    echo "// TPM Base Url" >> /var/www/html/config.php
	    echo "define('TPM_BASE_URL', '"${BASE_URL}"');" >> /var/www/html/config.php
	fi
fi

startTpm() {
    # Start apache
    echo "Starting apache2..."
    exec gosu root apache2-foreground
}

showHelp() {
    echo "Available options:"
    echo " tpm:start     - Starts the TPM server (default)"
    echo " help          - Displays this help"
    echo " [command]     - Execute the specified linux command eg. bash."
}

case "$1" in
    tpm:start)
        startTpm
        ;;
    help)
        showHelp
        ;;
    *)
        if [ -x $1 ]; then
            $1
        else
            prog=$(which $1)
            if [ -n "${prog}" ] ; then
                shift 1
                echo "executing $prog"
                $prog $@
            else
                help
            fi
        fi
        ;;
esac

exit 0
